'use strict';

/**
 * @ngdoc function
 * @name angularTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularTestApp
 */
angular.module('angularTestApp')
  .controller('MainCtrl', function ($scope, $http, $cookieStore, uiGmapGoogleMapApi, geolocation, orderAPIService, toastService, pusherService) {

    $scope.driver = {};
    $scope.orderDetails = {};

    uiGmapGoogleMapApi.then(function(maps) {

      geolocation.getLocation().then(function(data){

        $scope.map = {
          center: {
            latitude: data.coords.latitude,
            longitude: data.coords.longitude
          },
          zoom: 14
        };

        $scope.marker = {
          id: 0,
          options: {
            title: 'Your current address',
            animation: maps.Animation.DROP
          },
          coords: {
            latitude: data.coords.latitude,
            longitude: data.coords.longitude
          }
        };
      });
    });

    $scope.setOrderId = function(id){
      $cookieStore.put('orderId', id);
    };

    $scope.removeOrderId = function(){
      $cookieStore.remove('orderId');
    };

    $scope.getOrderId = function(){
      return $cookieStore.get('orderId');
    };

    $scope.setOrderDetails = function(order){
      $scope.orderDetails.to = order.to.formatted_address;
      $scope.orderDetails.from = order.from.formatted_address;
    };

    $scope.handleDriverDelay = function(){
      toastService.info('Unfortunatly you taxi will arrive later then expected');
    };

    $scope.setDriverDetails = function(driverDetails){
      $scope.driver.carNumber = driverDetails;
    };

    $scope.handleIsHere = function(){
      $scope.resetData();
      toastService.info('Your taxi is HERE !!!');
    };

    $scope.resetData = function(){
      pusherService.unsubscribe();
      $scope.driver = {};
      $scope.order = {};
      $scope.orderDetails = {};
      $scope.removeOrderId();
    };

    $scope.getCoordinates = function(order){

      var newOrder = 'Error';

      try{
        newOrder = {
          'lat_from': order.from.geometry.location.A,
          'lon_from': order.from.geometry.location.F,
          'lat_to': order.to.geometry.location.A,
          'lon_to': order.to.geometry.location.F,
          'from': order.from.formatted_address,
          'to' : order.to.formatted_address,
          'phone': order.phone
        };
      } catch (e) {
        toastService.error('Please check entered data');
      }

      return newOrder;
    };

    $scope.sendOrder = function(order) {

      var orderCoords = $scope.getCoordinates(order);

      if (orderCoords !== 'Error') {

        orderAPIService.sendOrder(orderCoords)
          .then(function (res) {
            $scope.setOrderId(res.id);
            $scope.setOrderDetails(order);
            pusherService.subscribe(res.id, 'Accept', $scope.setDriverDetails);
            pusherService.subscribe(res.id, 'Delay', $scope.handleDriverDelay);
            pusherService.subscribe(res.id, 'Finish', $scope.handleIsHere);
            toastService.info('Your order has been sent');
          }, function (err) {
            console.log(err);
            toastService.info('Error, try later');
          });
      }else{
        toastService.info('Sorry, wrong information');
      }
    };

    $scope.cancelOrder = function(){

      orderAPIService.cancelOrder($scope.getOrderId())
        .then(function(){
          $scope.resetData();
          toastService.info('Your order has been canceled' );
          $scope.removeOrderId();
        }, function(){
          toastService.info('Unfortunately it is too  late to cancel your order');
        });

    };

});
