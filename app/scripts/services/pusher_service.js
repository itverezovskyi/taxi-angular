'use strict';

angular.module('angularTestApp')
  .service('pusherService', function ($pusher){

    var pusherChannel, channelName;
    var service = this;
    var client = new Pusher('a9c9b3786868e1f5950b');
    var pusher = $pusher(client);

    service.subscribe = function(orderId, message, action){

      channelName = '' + orderId;
      pusherChannel = pusher.subscribe(channelName);

      pusherChannel.bind(message, function(data) {
        action(data);
      });
    };

    service.unsubscribe = function(){
      pusher.unsubscribe(channelName);
    };

    return service;
  });
