'use strict';

angular.module('angularTestApp')
  .service('orderAPIService', function($http, $q, $rootScope){

    var service = this;

    service.sendOrder = function(order){
      var defer = $q.defer();

      $http({
        method: 'POST',
        url: $rootScope.endPoint + '/order',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        transformRequest: function(obj) {
          var str = [];
          for(var p in obj) {
            str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
          }
          return str.join('&');
        },
        data: order
      })
      .success(function(res){
        defer.resolve(res);
      })
      .error(function(err){
        defer.reject(err);
      });

      return defer.promise;
    };

    service.cancelOrder = function(id){
      var defer = $q.defer();

      $http({
        method: 'GET',
        url: $rootScope.endPoint + '/order/' + id + '/cancel',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      })
      .success(function(res){
        defer.resolve(res);
      })
      .error(function(err){
        defer.reject(err);
      });

      return defer.promise;
    };


    return service;
  });
