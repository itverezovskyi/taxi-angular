'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('angularTestApp'));

  var MainCtrl,
    scope,
    deferred,
    promise,
    cookieStore,
    orderAPIService,
    toastService,
    pusherService,
    mockOrderAPIService,
    queryDeferred,
    q,
    backendResponse = {id: 12},
    googleADIResponseFrom = {'geometry':{'location':{'A':58.2229116,'F':26.389419599999997}},'formatted_address': 'Tartu'},
    googleADIResponseTo = {'geometry':{'location':{'A':58.3838253,'F':26.727496100000053}},'formatted_address': 'Kiev'},
    order = {from: googleADIResponseFrom, to: googleADIResponseTo, phone: '+380123'},
    wrongOrder = {from: googleADIResponseFrom, to: googleADIResponseTo, phone: 'wrongPhone'},
    correctRequest = {
      'lat_from': 58.2229116,
      'lon_from': 26.389419599999997,
      'lat_to': 58.3838253,
      'lon_to': 26.727496100000053,
      'from': 'Tartu',
      'to': 'Kiev',
      'phone': '+380123'
    };

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, _orderAPIService_, _toastService_, $q, _pusherService_, $cookieStore) {

    q = $q;
    deferred = $q.defer();
    promise = $q.defer().promise;

    scope = $rootScope.$new();
    cookieStore = $cookieStore;
    mockOrderAPIService = {
      sendOrder: function() {
        deferred = q.defer();
        return deferred.promise;
      },
      cancelOrder: function() {
        deferred = q.defer();
        return deferred.promise;
      }
    };

    MainCtrl = $controller('MainCtrl', {
      $scope: scope,
      orderAPIService: mockOrderAPIService
    });

    orderAPIService = _orderAPIService_;
    spyOn(orderAPIService, 'sendOrder').and.returnValue(q.when(correctRequest));
    spyOn(orderAPIService, 'cancelOrder').and.returnValue(q.when(correctRequest));



    pusherService = _pusherService_;
    spyOn(pusherService, 'subscribe');


    toastService = _toastService_;
    spyOn(toastService, 'info').and.returnValue(true);
    spyOn(toastService, 'error').and.returnValue(true);

    spyOn(scope, 'resetData');
    spyOn(scope, 'setOrderDetails');

  }));

  describe('cancel order', function(){

    beforeEach(inject(function($cookieStore){
      spyOn(mockOrderAPIService, 'cancelOrder').and.callFake(function () {

        deferred.resolve(backendResponse);

        return {
          then: function (callback){
            callback(deferred.promise);
          }
        };
      });

      scope.cancelOrder(12);
    }));

    it('should call reset data', inject(function () {
      expect(scope.resetData).toHaveBeenCalled();
    }));

    it('should show notification', inject(function () {
      expect(toastService.info).toHaveBeenCalled();
    }));
  });

  describe('submitOrder', function () {

    describe('general functionality', function () {

      beforeEach(inject(function($cookieStore){
        spyOn(mockOrderAPIService, 'sendOrder').and.callFake(function () {

          deferred.resolve(backendResponse);

          return {
            then: function (callback){
              callback(deferred.promise);
            }
          };
        });
      }));

      it('should call orderAPIService service', inject(function () {
        scope.sendOrder(order);
        expect(mockOrderAPIService.sendOrder).toHaveBeenCalled();
      }));

      it('should call send correct data', inject(function () {
        scope.sendOrder(order);
        expect(mockOrderAPIService.sendOrder).toHaveBeenCalledWith(correctRequest);
      }));

      it('should transform address to coordinates', inject(function(){
        var coordinatesJSON = scope.getCoordinates(order);

        expect(coordinatesJSON.lat_from).toBe(googleADIResponseFrom.geometry.location.A);
        expect(coordinatesJSON.lon_from).toBe(googleADIResponseFrom.geometry.location.F);

        expect(coordinatesJSON.lat_to).toBe(googleADIResponseTo.geometry.location.A);
        expect(coordinatesJSON.lon_to).toBe(googleADIResponseTo.geometry.location.F);

        expect(coordinatesJSON.from).toBe(googleADIResponseFrom.formatted_address);
        expect(coordinatesJSON.to).toBe(googleADIResponseTo.formatted_address);
      }));

      describe('coordinates handling', function(){

        describe('if cant convert address to coordinates', function() {

          beforeEach(inject(function(){
            scope.sendOrder({});
          }));

          it('should show notification that address is not recognized', inject(function () {
            expect(toastService.error).toHaveBeenCalled();
          }));

          it('should not execute POST to backend', function(){
            expect(mockOrderAPIService.sendOrder).not.toHaveBeenCalled();
          });

        });

        describe('if address is recognized', function() {

          beforeEach(inject(function(){
            spyOn(scope, "getCoordinates").and.callThrough();
            scope.sendOrder(order);
          }));

          it('should call transform address to coordinates', inject(function () {
            expect(scope.getCoordinates).toHaveBeenCalledWith(order);
          }));

          it('should transform coordinates correctly', function(){
            var result = scope.getCoordinates(order);
            expect(result).toEqual(correctRequest);
          });

          it('should send POST request to backend', function(){
            expect(mockOrderAPIService.sendOrder).toHaveBeenCalled();
          });
        });
      });

    });

    describe('when got success', function () {

      beforeEach(inject(function(){
        spyOn(scope, "setOrderId").and.callThrough();
        spyOn(mockOrderAPIService, 'sendOrder').and.callFake(function () {

          deferred.resolve(backendResponse);

          return {
            then: function (callback){
              callback(deferred.promise);
            }
          };
        });

        scope.sendOrder(order);

      }));

      it('should show success notification', inject(function () {
        expect(scope.setOrderId).toHaveBeenCalled();
      }));


      it('session id should not be defined', function(){
        var res = scope.getOrderId(order);
        expect(res).not.toBeDefined();
      });

      beforeEach(inject(function() {
        scope.sendOrder(order);
      }));

      it('should show success notification', inject(function () {
        expect(toastService.info).toHaveBeenCalled();
      }));

      it('should call function to save order id', inject(function(){
        expect(scope.setOrderId).toHaveBeenCalled();
      }));

      it('should show details', inject(function () {
        expect(scope.setOrderDetails).toHaveBeenCalled();
      }));

      it('should subscribe to order chanel', inject(function(){
        expect(pusherService.subscribe).toHaveBeenCalled();
      }));

      describe('and clicked send order again', function () {

        it('should show error notification', inject(function(){
          expect(toastService.info).toHaveBeenCalled();
        }));

        /*it('should not send another request to backend', inject(function(){

          var count = mockOrderAPIService.sendOrder.calls.count();
          scope.sendOrder(order);
          expect(mockOrderAPIService.sendOrder.calls.count()).toBe(count);
        }));*/

      });
    });

    describe('when got error', function () {

      it('should show error notification', inject(function () {
        scope.sendOrder({});
        expect(toastService.info).toHaveBeenCalled();
      }));
    });
  });
});

