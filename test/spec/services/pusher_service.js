'use strict';

describe("Pusher service", function () {



  var pusherService;
  var pusher;
  var $request = {from: 'Liivi 2', to: 'Narva mnt. 27', phone: '+234234234234'};
  var endPoint = 'https://taxi6.herokuapp.com';
  var testChannelName = '123';
  var testEvent = 'message';
  var testText = {message :'Your taxi will arrive in 3 minutes'};
  var testObj;

  beforeEach(module("angularTestApp"));

  beforeEach(inject(function (_pusherService_) {

    pusherService = _pusherService_;

    testObj = {
      testFunction: function (data) {
        return data;
      }
    };

    spyOn(pusherService, "subscribe").and.callThrough();
    spyOn(pusherService, "unsubscribe").and.callThrough();
    spyOn(testObj, "testFunction").and.callThrough();
  }));


 /* it('should execute subscribe', function() {
    pusherService.subscribe(testChannelName, testEvent, testObj.testFunction);
    expect(pusherService.subscribe).toHaveBeenCalled();
  });

  it('should execute subscribe', function() {
    pusherService.subscribe(testChannelName, testEvent, testObj.testFunction);
    Pusher.emit(testChannelName, testEvent, testText);
    expect(testObj.testFunction).toHaveBeenCalled();
  });

  it('should execute unsubscribe', function() {
    pusherService.unsubscribe();
    expect(pusherService.unsubscribe).toHaveBeenCalled();

    Pusher.emit(testChannelName, testEvent, testText);
    expect(testObj.testFunction).not.toHaveBeenCalled();
  });

  it('should execute action when emiting', function() {
    pusherService.subscribe(testChannelName, testEvent, testObj.testFunction);
    expect(testObj.testFunction).toHaveBeenCalled();
  });*/


});
