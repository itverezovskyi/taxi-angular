'use strict';

describe("Order API service", function () {

  var $httpBackend, $orderAPIService;

  var $request = {from: 'Liivi 2', to: 'Narva mnt. 27', phone: '+234234234234'};
  var endPoint = 'https://taxi6.herokuapp.com';
  var testId = 1;


  beforeEach(module("angularTestApp"));

  beforeEach(inject(function (_orderAPIService_, _$httpBackend_) {

    $httpBackend = _$httpBackend_;
    $orderAPIService = _orderAPIService_;

    $httpBackend.when('POST', endPoint + '/order').respond(201, {});
    $httpBackend.when('GET', endPoint + '/order/' + testId + '/cancel').respond(200, {});
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should have sent a POST request to server with right parameters with new request', function() {
    $orderAPIService.sendOrder($request);
    $httpBackend.expectPOST(endPoint + '/order').respond(201, '');
    $httpBackend.flush();
  });

  it('should have sent a GET request to server with order cancelation', function() {
    $orderAPIService.cancelOrder(testId);
    $httpBackend.expectGET(endPoint + '/order/' + testId + '/cancel').respond(200, '');
    $httpBackend.flush();
  });

});
